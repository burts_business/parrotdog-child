<?php get_header(); ?>
<div id="brewery-page" class="small-12 large-12" role="main">
	<div class="title-section homepage" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/brewery-background.jpg');">
		
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
</div>

<div class="row introduction">
	<h4><?php the_field('intro_title'); ?></h4>
	<p><?php the_field('intro_content'); ?></p>
	<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
</div>

<div class="row">
	<div class="flexslider">
	<?php if( have_rows('gallery') ):?>
		<ul class="slides">	
		<?php while ( have_rows('gallery') ) : the_row();?>
			<li>
	       		<?php $image = get_sub_field('image');
	        	if( !empty($image) ): ?>		
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php endif; ?>
			</li>
		<?php endwhile;
		else :?>
		</ul>
		<?php endif;?>	  		  
	</div>
</div>

<div class="row center images" style="padding-top:80px;">
	
	<h2><?php the_field('about_heading'); ?></h2>
	     <?php if (have_posts()) : ?>
	               <?php while (have_posts()) : the_post(); ?>    
	         
				   		<p><?php the_content(); ?></p>
	   
	               <?php endwhile; ?>
	     <?php endif; ?>

</div>

<div class="row" style="padding-bottom:80px;">
	<div class="small-12 large-6 left">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brewery-1.jpg" alt="brewery"/>
	</div>
	<div class="small-12 large-6 left">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brewery-5.jpg" alt="brewery"/>
	</div>
</div>

<div class="the-brewery clearfix">
		<div class="heading">
			<h2>Brewery <span class="demi"> Shop</span></h2>
		</div>
		<div class="info clearfix">
			<h1>BREWERY<br/> SHOP</h1>
			<a href="/brewery-shop/">
				<div class="button">
					<p>TAKE A LOOK AROUND</p>
				</div><!-- button -->
			</a>
		</div><!-- info -->
	</div>

		
<?php get_footer(); ?>
