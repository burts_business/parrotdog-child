<?php get_header(); ?>
<div id="contact" class="small-12 large-12" role="main">
	<div class="title-section homepage">
			<div class="intro-title">
				<h1>CONTACT</h1>
			</div>
		</div>
	
	
	<!--<div class="heading">
		<h2><i>Contact </i> <span class="demi">INFORMATION</span></h2>
	</div>-->
	<div class="row introduction">
		<h4>Contact Details</h4>
		<p>29 Vivian Street, Te Aro, Wellington 6011<br>
Phone: 04 384 8077<br>
<a href="mailto:info@parrotdog.co.nz">Email: info@parrotdog.co.nz</a></p>
		<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
	</div>
	<div class="sixty">
		<h2>General Enquiry</h2>
		<?php echo do_shortcode('[contact-form-7 id="101" title="General Enquiry"]'); ?>		
	</div>
	<div class="forty colour-35">
		<h2>Distribution Enquiry</h2>
		<?php echo do_shortcode('[contact-form-7 id="102" title="Distributor Enquiry"]'); ?>
	</div>

	
</div>
<?php get_footer(); ?>

