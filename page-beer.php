<?php get_header(); ?>
	

	<div id="beer-page">
		<div id="home" class="beer-hero beer-pages clearfix">
		
					
			<div id="beer-section" class="clear">
			<div class="row beer-page-title">
				<h1>BEER</h1>
				<p>Brewed year round, always enjoyable, always appropriate.<br> True to what we do - just. real. nice.</p>
				<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
			<div>
				<?php $args = array( 'post_type' => 'beers', 'category_name' => 'core', 'posts_per_page' => 5, 'order' => 'ASC', 'orderby' => 'date'  );
					$loop = new WP_Query( $args );?>
					
					<?php while ( $loop->have_posts() ) : $loop->the_post();?>
	
								<div class="mug-shot fifth">
								
								
								<?php 
		
									$image = get_field('main_image');
		
									if( !empty($image) ): ?>
		
									<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
		
								<?php endif; ?>
		
								
									<div class="beer-home home-hover colour-<?php the_ID(); ?>">
									<a href="<?php the_permalink();?>">
											<h3><?php the_title();?></h3>
										
											<div class="bottom-feeder">
												<p><span class="demi"><?php the_field('type');?> - <?php the_field('type/percentage'); ?></span></p>
												<hr>
												<p>Find out more</p>	
											</div>
										</a>			
									</div>
								</div>
								<?php endwhile; wp_reset_query(); ?>
							</div>
						</div>
			
		</div>	
	</div>
<?php get_footer(); ?>
