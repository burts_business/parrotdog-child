	<?php get_header('rare'); ?>
	<div id="rare-bird" class="small-12 large-12" role="main">
		<div class="title-section homepage clear">
			<div class="intro-title clear">
				<h1><img src="<?php bloginfo('stylesheet_directory'); ?>/images/rarebird.svg" alt="Rare Bird" title="Rare Bird"/></h1>
				<p>RareBird is a new series from ParrotDog releasing one-off beers inspired by the quirky birds of Aotearoa.</p>
				<p></p>
				<p>Extinction, or rarity, inherently brings about myth and legend.</p>
				<p>When you can no longer directly experience the creature, all you have are the second hand experiences, stories told by other people, the legends. And you wind up coming up with some kind of idea and image in your mind about what that thing would have been like. What you imagine it to be like. </p>
				<p></p>
				<p>Prompted by their interesting tales of existence; RareBird is a collaboration from ParrotDog working with artists and conservationists to share those stories. This series is about creativity in brewing and celebrating the strange characters in our endemic birdlife.</p>
				<p>Keep your eyes peeled for the RareBirds in special edition 650ml bottles</p>
				<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
			</div>
		</div>
	
		
		<div id="beer-section" class="clear" style="background: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/beer-bkg.jpg'); 	background-position-y: bottom; background-repeat: repeat-x; padding-bottom:50px; margin-top: -20px;">
			
			 <?php $args = array( 'post_type' => 'beers', 'category_name' => 'rare', 'posts_per_page' => -1, 'order' => 'DESC' );
			$loop = new WP_Query( $args );?>
			
			<?php while ( $loop->have_posts() ) : $loop->the_post();?>
			<a href="<?php the_permalink(); ?>">
			
					<div class="mug-shot">
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
					
					
					<div class="beer-home item home-hover colour-<?php the_ID(); ?>">
					
							<h3><?php the_field('homepage_name');?></h3>
						
							<div class="bottom-feeder">
								<p><span class="demi"><?php the_field('type');?> - <?php the_field('type/percentage'); ?></span></p>
								<hr>
								<p>Find out more</p>	
							</div>
							
					</div>
					</div>
					<?php endwhile; wp_reset_query(); ?>
			</a>
		
			
		</div><!-- beer section -->		
		<div>
			<div class="row introduction">
			
				
				
			</div>
		</div>	
		<div class="announcement center spacing">
				<h4>To hear about the rarest of releases, add your email below</h4>
				<div class="formwrap"><?php echo do_shortcode('[contact-form-7 id="662" title="Rarebird"]'); ?></div>
				<p><i>*all fields required</i></p>
			</div>
	</div>	
	<?php get_footer(); ?>
