<?php get_header('rare'); ?>
<div id="rare-beer">
	<div class="beer-hero clearfix background-image-<?php the_ID(); ?>">
		<img src="<?php the_field('rare_banner');?>" alt="Rarebird" />
		<div class="beer-info clearfix">
			<div class="left <?php the_field('text-col'); ?>">
				<img class="number" src="<?php the_field('rare_number');?>" alt="Rarebird" />
				<h2>Rarebird</h2>
				<h1 class="background-colour-<?php the_ID(); ?>"><?php the_field('homepage_name');?></h1>
				<p class="abv"><?php the_field('type/percentage'); ?></p>
				<p class="type background-colour-<?php the_ID(); ?>"><?php the_field('type');?></p>
				     <?php if (have_posts()) : ?>
				               <?php while (have_posts()) : the_post(); ?>    
				              
				<div class="excerpt"></div>
				          
			</div><!-- left -->
			<div class="right clearfix">
				
			</div><!-- right -->
		</div><!-- beer info -->
	
	</div><!-- hero -->
	 <?php endwhile; ?>
				  <?php endif; ?>
	<div class="divider"></div>
	
	<div class="rare-info clearfix">
		<div class="icons">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/monogram.svg" alt="Rare Bird" title="Rare Bird"/>
		</div>
		<div class="half">
			<?php the_content(); ?>
			
		</div>
		<div class="quarter beersies">
			<img src="<?php the_field('rare_bottle');?>" alt="Kakapo Beer" />
		</div>
	</div>
	
	
	
	<?php if( have_rows('rare_gallery') ):?>
	
		<div class="row clearfix">
			<h2><?php the_field('gallery_title'); ?></h2>
		  <div id="main-slider" class="flexslider">
	          <ul class="slides">
		          <?php	while ( have_rows('rare_gallery') ) : the_row(); ?>
		          <li>
					<div class="large-6 small-12 left">
						<img src="<?php the_sub_field('rare_gallery_image');?>" alt="rare bird" />
					</div>
					<div class="large-6 small-12 left module-right">
						<p><?php the_sub_field('rare_gallery_caption');?></p>
					</div>
		          </li>
		          <?php endwhile; ?>
	          </ul>
		  </div>
		</div>
		
		
		<?php	else :   // no rows found
	endif; ?>

	
	<div class="the-brewery clearfix">
		<div class="heading">
			<h2><i>The</i> <span class="demi">BREWERY</span></h2>
		</div>
		<div class="info clearfix">
			<h1>THE<br/>BREWERY</h1>
			<a href="/the-brewery/">
				<div class="button">
					<p>TAKE A LOOK AROUND</p>
				</div><!-- button -->
			</a>
		</div><!-- info -->
	</div>
</div>

<?php get_footer(); ?>