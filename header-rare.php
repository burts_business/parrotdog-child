<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php if ( is_category() ) {
      echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_tag() ) {
      echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_archive() ) {
      wp_title(''); echo ' Archive | '; bloginfo( 'name' );
    } elseif ( is_search() ) {
      echo 'Search for &quot;'.esc_html($s).'&quot; | '; bloginfo( 'name' );
    } elseif ( is_home() || is_front_page() ) {
      bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
    }  elseif ( is_404() ) {
      echo 'Error 404 Not Found | '; bloginfo( 'name' );
    } elseif ( is_single() ) {
      wp_title('');
    } else {
      echo wp_title( ' | ', 'false', 'right' ); bloginfo( 'name' );
    } ?></title>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.pageslide.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/rare.css" />
    
    
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-precomposed.png">
    
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/webfonts.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/MyFontsWebfontsKit.css" />
    
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".secret").click(function(){
    $("#overlay").hide();
  });
  $("#show").click(function(){
    $("p").show();
  });
});

</script>
    
   
  <script src="//use.typekit.net/hdt4wqc.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/flexslider.css" type="text/css" media="screen" />
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57815724-1', 'auto');
  ga('send', 'pageview');

</script>
	
    
    <?php wp_head(); ?>
  </head>
  <body id="post-<?php the_ID(); ?>">

  <div class="off-canvas-wrap">
	  <div class="inner-wrap">  
	  	<header>
	  		<div id="logo" <?php body_class( $class ); ?>>
		  		
	  			<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="ParrotDog"/></a>
	  		</div>
	  	
	  	<a href="https://www.facebook.com/parrotdog" class="facebook"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="facebook"/>
			  	<a href="https://twitter.com/parrotdog" class="twitter"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.svg" alt="twitter" />
			  	<a href="http://instagram.com/parrotdogbeer" class="instagram"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/instagram.svg" alt="instagram" />	
	  	<a id="starter" href="#modal" class="second-this menu"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/menu.svg" alt="menu"/></a>
	  		<div id="modal" style="display:none;">	
			  	<nav class="tab-bar show-for-small-only">
			  	 <section class="top-bar-section">
				   <ul id="menu-secondary" class="top-bar-menu right">
					   <li class="divider"></li><li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a href="http://parrotdog.co.nz/beer/">Beer</a></li>
			<li class="divider"></li><li id="menu-item-94" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-94"><a href="http://parrotdog.co.nz/limited-release/">Limited Releases</a></li>
					<li class="divider"></li><li id="menu-item-93" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-93"><a href="http://parrotdog.co.nz/rarebird/">Rarebird</a></li>
			<li class="divider"></li><li id="menu-item-169" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-169"><a href="http://parrotdog.co.nz/our-story/">Our Story</a></li>
			<li class="divider"></li><li id="menu-item-154" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154"><a href="http://parrotdog.co.nz/the-brewery/">The Brewery</a></li>
			<li class="divider"></li><li id="menu-item-196" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-196"><a href="http://parrotdog.co.nz/brewery-shop/">Brewery Shop</a></li>
			<li class="divider"></li><li id="menu-item-161" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-161"><a href="http://parrotdog.co.nz/shop/">Shop Online</a></li>
			<li class="divider"></li><li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a href="http://parrotdog.co.nz/news/">News</a></li>
					</ul>
			  	 </section>
			  	</nav>
		
		 
		  
		        <div class="top-bar-container contain-to-grid show-for-small-up">
		            <nav class="top-bar" data-topbar="">
		            
		                <section class="top-bar-section">

		                    <?php foundationPress_top_bar_r(); ?>
		                </section>
		            </nav>
		        </div>

			  	<!--<a href="https://www.facebook.com/parrotdog" class="facebook"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="facebook"/>
			  	<a href="https://twitter.com/parrotdog" class="twitter"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.svg" alt="twitter" />-->
			  	<a href="<?php echo home_url(); ?>" class="instagram home-icon"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/home.svg" alt="instagram" /></a>
			  	<a id="harvey" class="close" href="javascript:$.pageslide.close()"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/close.svg" alt="menu close"/></a>
		  	</div>
		  	
		
		  	  	</header>
		<section class="container" role="document">