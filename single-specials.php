<?php get_header(); ?>


<div class="beer-hero hero-special">
	<div class="specials" style="padding-top:0px;">
		<?php
				$image = get_field('special_banner');

				if( !empty($image) ): ?>

					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
	</div>
	
</div><!-- hero -->


<div class="order-online padding-neutral clearfix">

		<div class="left">
			<p class="limited-<?php the_ID(); ?> limited">limited releases</p>
			<p class="type background-colour-<?php the_ID(); ?>"><?php the_field('type');?></p>
			<p class="abv"><?php the_field('type/percentage'); ?></p>
			
			    <?php if (have_posts()) : ?>
			        <?php while (have_posts()) : the_post(); ?>    
			              
					<div class="excerpt">
						<p><?php the_content(); ?></p>	
						
					</div>
			          
		</div><!-- left -->
		<div class="right clearfix">
			
			<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail();
			} ?>
			
		</div><!-- right -->
					<?php endwhile; ?>
				<?php endif; ?>
			  
		<div class="clearfix" style="margin-bottom:60px;"></div>
			  
		<div class="left clearfix">
			<p>Availability:</p>
			<div class="icons clearfix">
				<div class="single <?php if(get_field('single') == "Not Available"){echo "not-available";};?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/beer.svg" alt="Single">
					<p>Singles</p>
				</div>
				<div class="packs <?php if(get_field('packs') == "Not Available"){echo "not-available";};?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/box.svg" alt="Pack">
					<p>Packs</p>
				</div>
				<!--<div class="tap <?php if(get_field('on_tap') == "Not Available"){echo "not-available";};?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
					<p>On Tap</p>
				</div>-->
				<div class="keg <?php if(get_field('keg') == "Not Available"){echo "not-available";};?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/keg.svg" alt="Keg">
					<p>Kegs</p>
				</div>
			</div><!-- icons -->
			<a href="<?php the_field('order_link');?>">
				<div class="button hover-background-colour-<?php the_ID(); ?>">
					<p class="background-colour-<?php the_ID(); ?>">ORDER ONLINE</p>
				</div><!-- button -->
			</a>
			<a href="<?php echo home_url(); ?>/beer-awards/">
				<img class="awards-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/wards.svg" alt="awards" title="See all our Awards" />
			</a>
			
			
			
		</div><!-- left -->
		<div class="right clearfix">
			<div class="quote">
				<h1 class="background-colour-<?php the_ID(); ?>">"<?php the_field('quote');?>"</h1>
				<p class="person">- <?php the_field('quote_by');?></p>
			</div><!-- quote -->
		</div><!-- right -->

	</div><!-- order online -->

<!--<div class="instafeed-container clearfix">
<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
		
	<div class="overlay">
		<h1>#<?php the_title(); ?><br>@PARROTDOGBEER</h1>
	</div><!-- overlay -->
</div><!-- instafeed container -->




<?php get_footer(); ?>