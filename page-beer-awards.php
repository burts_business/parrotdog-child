<?php get_header(); ?>
<div id="awards" class="small-12 large-12" role="main">
	<div class="title-section homepage">
		<div class="intro-title">
			<h1>AWARDS</h1>
		</div>
	</div>
	<div class="row introduction">
		<img class="award" width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/awards-logo.svg" alt="decal"/>
		<p>We don't like to boast, but we do like to celebrate. Here are a couple of our achievements from the last couple of years.</p>
		<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
	</div>
	<div class="row clearfix tupperware">
		<div class="large-12">
			<?php if (have_posts()) :
				 while (have_posts()) : the_post(); 
					 the_content();
				 endwhile;
			endif; ?>
		</div>		
	</div>

	<div class="the-brewery clearfix">
		<div class="heading">
			<h2>The <span class="demi">Brewery</span></h2>
		</div>
		<div class="info clearfix">
			<h1>THE<br/>BREWERY</h1>
			<a href="/the-brewery/">
				<div class="button">
					<p>TAKE A LOOK AROUND</p>
				</div><!-- button -->
			</a>
		</div><!-- info -->
	</div>
</div>
<?php get_footer(); ?>

