<?php get_header(); ?>
	<div id="commercials" class="small-12 large-12" role="main">
		<div class="title-section homepage">
			<div class="intro-title">
				<h1>COMMERCIALS</h1>
			</div>
		</div>
		
		
		<div class="com-tup">
			<div class="row clearfix tupperware">
				<div class="large-8 left">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/80409218' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>
				<div class="large-4 left module-right">
					<h2>New Human</h2>
					<p><span class="demi">Oop, there he is, hiding in the brushes, searching for berries. Look at him. He's brand new.</span></p>
					<p>Check out our nice new advert created by the very talented Sam Krispstifski from Curious Films</p>
					
				</div>
			</div>
			
		
			<div class="row clearfix tupperware">
				
				<div class="large-8 left">
						<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='http://www.youtube.com/embed/-eOvLbdW7Qo' frameborder='0' allowfullscreen></iframe></div>					
				</div>
				<div class="large-4 left module-right">
					<h2>SLIDE</h2>
					<p><span class="demi">Oop, there he is, hiding in the brushes, searching for berries. Look at him. He's brand new.</span></p>
					<p>Check out our nice new advert created by the very talented Sam Krispstifski from Curious Films</p>
								</div>
			</div>
		</div>

	</div>
		
<?php get_footer(); ?>