<?php 

add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'beers',
		array(
			'labels' => array(
				'name' => __( 'Beer' ),
				'singular_name' => __( 'Beer' )
			),
		'public' => true,
		'hierarchical' => false,
		'supports' => array('title','editor','thumbnail'),
        'taxonomies' => array('category')
		)
	);
}
?>
<?php
function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}
?>
<?php
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(STYLESHEETPATH . "/single-{$cat->slug}.php") )
		return STYLESHEETPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);
?>
<?php
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);
function my_theme_wrapper_start() {
  echo '<div id="woocommerce" class="small-12 large-12" role="main">
		<div class="title-section homepage">
			<div class="intro-title">
				<h1>SHOP</h1>
			</div>
		</div>
	</div>
	<div id="about-section" class="clear">
			<div class="row">';
}
function my_theme_wrapper_end() {
  echo '</div><!--End of About Section-->
  		</div>';
}
?>
<?php
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] ); 			// Remove the reviews tab
    return $tabs;
}
?>
<?php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 999;' ), 20 );
/**
 * Add the field to the checkout
 */
add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );
 
function my_custom_checkout_field( $checkout ) {
 
    echo '<div id="my_custom_checkout_field"><h3>' . __('Warning - It is against the law to sell or supply alcohol to a person under the age of 18 years.') . '</h3>';
 echo '<p>' . __(' At ParrotDog it is required that any purchaser or acceptor of alcohol purchased through our online store confirms that they are aged 18 years or older. The customer must also declare that they are not under the influence of alcohol at the time the order is submitted or at the point the order signed for upon delivery.') . '</p>';
 
    woocommerce_form_field( 'r-eighteen', array(
        'type'          => 'checkbox',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => __('By ticking the box to the left you hearby accept our terms and declare the above.'),
        'placeholder'   => __('Enter something'),
        ), $checkout->get_value( 'r-eighteen' ));
 
    echo '</div>';
 
}
/**
 * Process the checkout
 */
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
 
function my_custom_checkout_field_process() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['r-eighteen'] )
        wc_add_notice( __( 'You must tick you are over 18 to purchase off this website' ), 'error' );
}
/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );
 
function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['r-eighteen'] ) ) {
        update_post_meta( $order_id, 'Are you 18?', sanitize_text_field( $_POST['r-eighteen'] ) );
    }
}
/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('r-eighteen').':</strong> ' . get_post_meta( $order->id, 'Are you 18?', true ) . '</p>';
}
?>
<?php
add_theme_support( 'woocommerce' );
?>
<?php
add_image_size( 'custom-size', 220, 220, array( 'left', 'top' ) ); // Hard crop left top
/*
* replace read more buttons for out of stock items
**/
if (!function_exists('woocommerce_template_loop_add_to_cart')) {
function woocommerce_template_loop_add_to_cart() {
global $product;
if (!$product->is_in_stock()) {
    echo '<a href="'.get_permalink().'" rel="nofollow" class="outstock_button button product_type_simple">Out of Stock</a>';
} 
else
{ 
    woocommerce_get_template('loop/add-to-cart.php');
}
}
}
add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
function woo_custom_breadrumb_home_url() {
    return 'http://parrotdog.co.nz/shop';
}
add_filter( 'add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // < 2.1
function woo_archive_custom_cart_button_text() {
        return __( 'My Button Text', 'woocommerce' );
}
function mv_my_theme_scripts()
{
wp_enqueue_script('add-to-cart-variation', plugins_url() . '/woocommerce/assets/js/frontend/add-to-cart-variation.js',array('jquery'),'1.0',true);
}
add_action('wp_enqueue_scripts','mv_my_theme_scripts');
?>
