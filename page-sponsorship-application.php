<?php get_header(); ?>
<div id="brewery-page" class="small-12 large-12" role="main">
	<div class="title-section homepage" style="background-image: url('http://parrotdog.co.nz/wp-content/themes/parrotdog-child/images/brewery-background.jpg');">
		
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
</div>

<div class="row introduction">
	<!--<h4>To apply for sponsorship please download the form below and send it to us via email or post. </h4>
	<p><i>Please note that applications should be received one month in advance to give us time to process your request.</i></p>
	<p>29 Vivian Street <br/>
		Te Aro Wellington <br/>
		6011<br/>
		<b>Phone:</b> 04 384 8077 <br/>
		<b>Email:</b> info@parrotdog.co.nz<br/>
	</p>-->
	<h4>Thank you for considering ParrotDog as your sponsorship partner. ParrotDog is always interested in supporting worthy causes, events and companies in both our local and wider communities.</h4>
	<p>Recently we have supported art exhibitions, film festivals, fashion shows, music events, school fundraisers, local entrepreneurs, emerging companies, and various charities.</p>
	<p>ParrotDog receives a vast number of requests for sponsorship but unfortunately we cannot support every applicant. ParrotDog is brewed using expensive ingredients, which means the cost of producing our product is very high. We therefore offer our product at a discounted rate rather than free of charge, to those we wish to support.</p>
	<p>If you would like to apply for sponsorship please provide answers to the following questions via email or post. Please note that applications should be received one month in advance to give us time to process your request.</p>
	<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
</div>


<div class="row center images" style="padding-top:20px;">
	<div class="sponsorship-form">
		<?php echo do_shortcode('[contact-form-7 id="727" title="Sponsorship Application"]'); ?>
	</div>
	
	<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
	<h4>Alternatively download the form below and send it to us via email or post. </h4>
	<p><i>Please note that applications should be received one month in advance to give us time to process your request.</i></p>
	<p>29 Vivian Street <br/>
		Te Aro Wellington <br/>
		6011<br/>
		<b>Phone:</b> 04 384 8077 <br/>
		<b>Email:</b> info@parrotdog.co.nz<br/>
	</p>

	
<a href="http://parrotdog.co.nz/wp-content/themes/parrotdog-child/assets/sponsorship-application.pdf" class="button">Download PDF</a>

</div>



<div class="the-brewery clearfix">
		<div class="heading">
			<h2><i>Brewery</i> <span class="demi"> SHOP</span></h2>
		</div>
		<div class="info clearfix">
			<h1>BREWERY<br/> SHOP</h1>
			<a href="/brewery-shop/">
				<div class="button">
					<p>TAKE A LOOK AROUND</p>
				</div><!-- button -->
			</a>
		</div><!-- info -->
	</div>

		
<?php get_footer(); ?>
