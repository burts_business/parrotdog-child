<?php get_header(); ?>
	<div class="small-12 large-12" role="main">
		<div class="title-section homepage" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/header-image-beer.jpg');">
			<div class="intro-title">
				<h1>NEWS</h1>
			</div>
		</div>
		
		<div class="dark clearfix">
			<div class="row">
				
		
			
			  	 <?php $args = array( 'posts_per_page' => -1, 'order' => 'DESC' );
					$loop = new WP_Query( $args );?>
					<?php while ( $loop->have_posts() ) : $loop->the_post(); $count++;?>
					

   
			               
			               <div class="large-4 news">
							   <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {
							   the_post_thumbnail();
							   } ?></a>
							   <div class="news-container">
							   <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								   <?php 
										if ( in_category( 'events' )) {
											echo '<a href="/category/events"><h4>Events</h4></a>'; 
											
											} elseif ( in_category( 'promotions' )) {
												echo '<a href="/category/promotions"><h4>Promotions</h4></a>';
											
											} else {
												echo "<h4>News</h4>";
										}
									?>
									<h3><?php the_title(); ?></h3>
									<p><?php $excerpt = get_the_excerpt();
										  echo string_limit_words($excerpt,15);?>
									</p>
								   <a href="<?php the_permalink(); ?>"><p class="button">Read More</p></a>
							   </div>
							   </div>
						   </div>

			               
						<?php  if ( 0 == $count%3 ) {
						        echo '<div class="clear"></div>';
						    }
						endwhile; //ending the loop
						if ( 0 != $count%3 ) {
						   echo '<div class="clear"></div>';
						}?>

			
			
			</div>
		</div>
	

	</div>
		
<?php get_footer(); ?>